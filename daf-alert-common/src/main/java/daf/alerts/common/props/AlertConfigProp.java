package daf.alerts.common.props;

import daf.alerts.common.models.Alert;
import daf.alerts.common.models.Payload;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.util.OutputTag;

public class AlertConfigProp {

    public static final String DRIVER          = "postgres.driver.class.name";
    public static final String JDBC_URL        = "postgres.jdbc.url";
    public static final String DATABASE        = "postgres.map.database";
    public static final String DATABASE2        = "postgres.threshold.database";
    public static final String USERNAME        = "postgres.username";
    public static final String PASSWORD        = "postgres.password";

    public static final String KAFKA_GRP_ID = "group.id";
    public static final String KAFKA_BOOTSTRAP_SERVER = "bootstrap.servers";
    public static final String KAFKA_AUTO_OFFSET = "auto.offset.reset";
    public static final String KAFKA_DAF_ALERT_MAPPING_TOPIC = "daf.alert.mapping.topic";
    public static final String KAFKA_DAF_STATUS_MSG_TOPIC = "daf.status.topic";
    public static final String KAFKA_DAF_ALERT_PRODUCE_MSG_TOPIC = "daf.alert.produce.topic";

    public static final String ALERT_MAP_FETCH_QUERY="postgres.alert.map.fetch.query";
    public static final String ALERT_THRESHOLD_FETCH_QUERY="postgres.alert.threshold.fetch.query";
    public static final String ALERT_THRESHOLD_FETCH_SINGLE_QUERY="postgres.threshold.fetch.query";

    public static final String POSTGRESS_SINGLE_VEHICLE_QUERY="postgres.single.vehicle.fetch.query";
    public static final String POSTGRESS_GROUP_VEHICLE_QUERY="postgres.group.vehicle.fetch.query";


    public static final TypeInformation<?>[] ALERT_THRESHOLD_SCHEMA_DEF = new TypeInformation<?>[] {
            BasicTypeInfo.INT_TYPE_INFO,
            BasicTypeInfo.INT_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.BIG_DEC_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO
    };

    public static final TypeInformation<?>[] ALERT_MAP_SCHEMA_DEF = new TypeInformation<?>[] {
            BasicTypeInfo.INT_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.INT_TYPE_INFO};

    public static final TypeInformation<?>[] ALERT_DB_VIN_THRESHOLD_SCHEMA_DEF = new TypeInformation<?>[] {
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.INT_TYPE_INFO,
            BasicTypeInfo.INT_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO,
            BasicTypeInfo.STRING_TYPE_INFO
    };

    public  static final MapStateDescriptor<String, Payload> alertMappingSchemaMapStateDescriptor = new MapStateDescriptor("alertMappingSchemaMapStateDescriptor", String.class,Payload.class);
    public static final OutputTag<Alert> OUTPUT_TAG = new OutputTag<Alert>("side-output") {};
}
