package daf.alerts.common.models;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Payload {

    private Optional<Object> data;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @Setter(AccessLevel.NONE)
    private final Long createdTimestamp = System.currentTimeMillis();
}
