package daf.alerts.common.models.schema;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class VehicleAlertRefSchema implements Serializable{

    @JsonProperty("id")
    private Long id;
    @JsonProperty("alertId")
    private Long alertId;
    @JsonProperty("state")
    private String state;
    @JsonProperty("vin")
    private String vin;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private Long timestamp = System.currentTimeMillis();
    private final static long serialVersionUID = -983704599562554769L;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public VehicleAlertRefSchema withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("alertId")
    public Long getAlertId() {
        return alertId;
    }

    @JsonProperty("alertId")
    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    public VehicleAlertRefSchema withAlertId(Long alertId) {
        this.alertId = alertId;
        return this;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    public VehicleAlertRefSchema withState(String state) {
        this.state = state;
        return this;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public VehicleAlertRefSchema withVin(String vin) {
        this.vin = vin;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public VehicleAlertRefSchema withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }
}
