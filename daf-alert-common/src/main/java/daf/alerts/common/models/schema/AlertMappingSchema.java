package daf.alerts.common.models.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AlertMappingSchema {
    @JsonProperty("Id")
    private Integer id;
    @JsonProperty("vin")
    private String vin;
    @JsonProperty("alertId")
    private Integer alertId;

    private Long timestamp = System.currentTimeMillis();
}
