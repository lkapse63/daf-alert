package daf.alerts.common.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Optional;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Source implements Serializable {

    private static final long serialVersionUID = 3482505461901742360L;

    private MetaData metaData;
    private Optional<Object> payload;
}
