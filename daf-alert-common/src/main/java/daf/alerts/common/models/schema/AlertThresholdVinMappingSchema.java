package daf.alerts.common.models.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AlertThresholdVinMappingSchema {

    private Integer id;
    private String vin;
    private String vid;
    private Integer alertId;
    private Double thresholdValue;
    private String unitType;
    private String urgencyLevelType;
    private String alertName;
    private String alertNameType;
    private String parentEnum;
}
