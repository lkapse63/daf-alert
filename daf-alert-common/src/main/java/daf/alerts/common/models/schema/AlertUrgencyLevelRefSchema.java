package daf.alerts.common.models.schema;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class AlertUrgencyLevelRefSchema implements Serializable{
    @JsonProperty("id")
    private Long id;
    @JsonProperty("alert_id")
    private Long alertId;
    @JsonProperty("urgency_level_type")
    private String urgencyLevelType;
    @JsonProperty("threshold_value")
    private Long thresholdValue;
    @JsonProperty("unit_type")
    private String unitType;
    @JsonProperty("day_type")
    private Long dayType;
    @JsonProperty("period_type")
    private String periodType;
    @JsonProperty("urgencylevel_start_date")
    private Long urgencylevelStartDate;
    @JsonProperty("urgencylevel_end_date")
    private Long urgencylevelEndDate;
    @JsonProperty("state")
    private String state;
    @JsonProperty("alertTypeName")
    private String alertTypeName;
    @JsonProperty("alertTypeName")
    private String alertName;

    @JsonProperty("created_at")
    private Long createdAt;
    @JsonProperty("modified_at")
    private Long modifiedAt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private Long timestamp = System.currentTimeMillis();
    private final static long serialVersionUID = 3317314917107066075L;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public AlertUrgencyLevelRefSchema withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("alert_id")
    public Long getAlertId() {
        return alertId;
    }

    @JsonProperty("alert_id")
    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    public AlertUrgencyLevelRefSchema withAlertId(Long alertId) {
        this.alertId = alertId;
        return this;
    }

    @JsonProperty("urgency_level_type")
    public String getUrgencyLevelType() {
        return urgencyLevelType;
    }

    @JsonProperty("urgency_level_type")
    public void setUrgencyLevelType(String urgencyLevelType) {
        this.urgencyLevelType = urgencyLevelType;
    }

    public AlertUrgencyLevelRefSchema withUrgencyLevelType(String urgencyLevelType) {
        this.urgencyLevelType = urgencyLevelType;
        return this;
    }

    @JsonProperty("threshold_value")
    public Long getThresholdValue() {
        return thresholdValue;
    }

    @JsonProperty("threshold_value")
    public void setThresholdValue(Long thresholdValue) {
        this.thresholdValue = thresholdValue;
    }

    public AlertUrgencyLevelRefSchema withThresholdValue(Long thresholdValue) {
        this.thresholdValue = thresholdValue;
        return this;
    }

    @JsonProperty("unit_type")
    public String getUnitType() {
        return unitType;
    }

    @JsonProperty("unit_type")
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public AlertUrgencyLevelRefSchema withUnitType(String unitType) {
        this.unitType = unitType;
        return this;
    }

    @JsonProperty("day_type")
    public Long getDayType() {
        return dayType;
    }

    @JsonProperty("day_type")
    public void setDayType(Long dayType) {
        this.dayType = dayType;
    }

    public AlertUrgencyLevelRefSchema withDayType(Long dayType) {
        this.dayType = dayType;
        return this;
    }

    @JsonProperty("period_type")
    public String getPeriodType() {
        return periodType;
    }

    @JsonProperty("period_type")
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public AlertUrgencyLevelRefSchema withPeriodType(String periodType) {
        this.periodType = periodType;
        return this;
    }

    @JsonProperty("urgencylevel_start_date")
    public Long getUrgencylevelStartDate() {
        return urgencylevelStartDate;
    }

    @JsonProperty("urgencylevel_start_date")
    public void setUrgencylevelStartDate(Long urgencylevelStartDate) {
        this.urgencylevelStartDate = urgencylevelStartDate;
    }

    public AlertUrgencyLevelRefSchema withUrgencylevelStartDate(Long urgencylevelStartDate) {
        this.urgencylevelStartDate = urgencylevelStartDate;
        return this;
    }

    @JsonProperty("urgencylevel_end_date")
    public Long getUrgencylevelEndDate() {
        return urgencylevelEndDate;
    }

    @JsonProperty("urgencylevel_end_date")
    public void setUrgencylevelEndDate(Long urgencylevelEndDate) {
        this.urgencylevelEndDate = urgencylevelEndDate;
    }

    public AlertUrgencyLevelRefSchema withUrgencylevelEndDate(Long urgencylevelEndDate) {
        this.urgencylevelEndDate = urgencylevelEndDate;
        return this;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    public AlertUrgencyLevelRefSchema withState(String state) {
        this.state = state;
        return this;
    }

    @JsonProperty("created_at")
    public Long getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public AlertUrgencyLevelRefSchema withCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("modified_at")
    public Long getModifiedAt() {
        return modifiedAt;
    }

    @JsonProperty("modified_at")
    public void setModifiedAt(Long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public AlertUrgencyLevelRefSchema withModifiedAt(Long modifiedAt) {
        this.modifiedAt = modifiedAt;
        return this;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AlertUrgencyLevelRefSchema withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public String getAlertTypeName() {
        return alertTypeName;
    }

    public void setAlertTypeName(String alertTypeName) {
        this.alertTypeName = alertTypeName;
    }

    public AlertUrgencyLevelRefSchema withAlertTypeName(String alertTypeName){
        this.alertTypeName=alertTypeName;
        return this;
    }

    public String getAlertName() {
        return alertName;
    }

    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    public AlertUrgencyLevelRefSchema withAlertName(String alertName){
        this.alertName=alertName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlertUrgencyLevelRefSchema that = (AlertUrgencyLevelRefSchema) o;
        return getAlertId().equals(that.getAlertId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAlertId());
    }
}