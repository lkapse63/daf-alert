package daf.alerts.common.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Optional;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Target implements Serializable {
    private static final long serialVersionUID = -8957861647045201073L;

    private MetaData metaData;
    private Optional<Object> payload;
    private Optional<Alert> alert;
}
