package daf.alerts.common.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MetaData implements Serializable {
    private static final long serialVersionUID = -4683496582524846984L;

    private Optional<Map<Object,Object>> config;
    private Optional<Object> threshold;

}
