package daf.alerts.common.util;

import daf.alerts.common.models.Payload;
import daf.alerts.common.models.conti.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UtilsTest {

    @Test
    public void round() {
        double round = Utils.round(123.4554, 2);
        assertEquals(123.45,round,0.5);
    }

    @Test
    public void writeValueAsString() {
        String test_data = Utils.writeValueAsString(Payload.builder().data(Optional.of("test_data")).build());
        assertNotNull(test_data);
    }

    @Test
    public void readValueAsObject() throws Exception {
        String statusJson = "{\"GPSStartLatitude\": \"51.43042755\",\"VStartFuel\": \"116939374\",\"Increment\": \"54684\",\"GPSStartLongitude\": \"5.51616478\",\"DocFormat\": \"json\",\"EventDateTimeFirstIndex\": \"2021-05-03T13:11:31.000Z\",\"TransID\": \"03010\",\"GPSEndLongitude\": \"5.508029461\",\"TenantID\": \"DAF_4A\",\"VStartTankLevel\": \"55\",\"DocVersion\": \"1.3\",\"Reserve0\": \"10000\",\"GPSStopVehDist\": \"418215680\",\"ROProfil\": \"BASE_CONTI\",\"ROrelease\": \"13.1.1_M4A\",\"VStopTankLevel\": \"55\",\"VNegAltitudeVariation\": \"6\",\"Jobname\": \"acco07\",\"GPSStartVehDist\": \"418214765\",\"VPosAltitudeVariation\": \"0\",\"GPSEndDateTime\": \"2021-05-03T13:14:02.000Z\",\"NumSeq\": \"997\",\"VUsedFuel\": \"342\",\"VCruiseControlDist\": \"0\",\"ROmodel\": \"TCU Conti Flex 4A\",\"VHarshBrakeDuration\": \"5\",\"VPTOCnt\": \"0\",\"TargetM2M\": \"MOM_CBP\",\"VStopFuel\": \"116939716\",\"Reserve1\": \"1\",\"PartitionID\": \"8\",\"DriverID\": \"NL B000384974000000\",\"VBrakeDuration\": \"17\",\"ROName\": \"CONTI_FLEX_181812081514311400938\",\"VEvtID\": \"5\",\"GPSEndLatitude\": \"51.43199539\",\"GPSStartDateTime\": \"2021-05-03T13:11:00.000Z\",\"NumberOfIndexMessage\": \"2\",\"VID\": \"vin1\",\"VIdleDuration\": \"14\",\"VPTODuration\": \"0\",\"VPTODist\": \"0\",\"messageKey\": \"M4A1117_1620047672_03010\",\"receivedTimestamp\": 1620047672650,\"hostname\": \"tsclba64s\",\"partitionId\": \"8\",\"queue\": \"MSGTOCBP08_4A\"}";
        Status status = (Status) Utils.readValueAsObject(statusJson, Status.class);
        assertNotNull(status);
        assertEquals("51.43042755", status.getGPSStartLatitude());
    }

    @Test
    public void testReadValueAsObject() throws Exception {
        String statusJson = "{\"GPSStartLatitude\": \"51.43042755\",\"VStartFuel\": \"116939374\",\"Increment\": \"54684\",\"GPSStartLongitude\": \"5.51616478\",\"DocFormat\": \"json\",\"EventDateTimeFirstIndex\": \"2021-05-03T13:11:31.000Z\",\"TransID\": \"03010\",\"GPSEndLongitude\": \"5.508029461\",\"TenantID\": \"DAF_4A\",\"VStartTankLevel\": \"55\",\"DocVersion\": \"1.3\",\"Reserve0\": \"10000\",\"GPSStopVehDist\": \"418215680\",\"ROProfil\": \"BASE_CONTI\",\"ROrelease\": \"13.1.1_M4A\",\"VStopTankLevel\": \"55\",\"VNegAltitudeVariation\": \"6\",\"Jobname\": \"acco07\",\"GPSStartVehDist\": \"418214765\",\"VPosAltitudeVariation\": \"0\",\"GPSEndDateTime\": \"2021-05-03T13:14:02.000Z\",\"NumSeq\": \"997\",\"VUsedFuel\": \"342\",\"VCruiseControlDist\": \"0\",\"ROmodel\": \"TCU Conti Flex 4A\",\"VHarshBrakeDuration\": \"5\",\"VPTOCnt\": \"0\",\"TargetM2M\": \"MOM_CBP\",\"VStopFuel\": \"116939716\",\"Reserve1\": \"1\",\"PartitionID\": \"8\",\"DriverID\": \"NL B000384974000000\",\"VBrakeDuration\": \"17\",\"ROName\": \"CONTI_FLEX_181812081514311400938\",\"VEvtID\": \"5\",\"GPSEndLatitude\": \"51.43199539\",\"GPSStartDateTime\": \"2021-05-03T13:11:00.000Z\",\"NumberOfIndexMessage\": \"2\",\"VID\": \"vin1\",\"VIdleDuration\": \"14\",\"VPTODuration\": \"0\",\"VPTODist\": \"0\",\"messageKey\": \"M4A1117_1620047672_03010\",\"receivedTimestamp\": 1620047672650,\"hostname\": \"tsclba64s\",\"partitionId\": \"8\",\"queue\": \"MSGTOCBP08_4A\"}";
        Status status = (Status) Utils.readValueAsObject(statusJson, Status.class);
        assertNotNull(status);
        assertEquals("51.43042755", status.getGPSStartLatitude());
    }
}