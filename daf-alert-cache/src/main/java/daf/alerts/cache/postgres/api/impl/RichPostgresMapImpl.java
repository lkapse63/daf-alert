package daf.alerts.cache.postgres.api.impl;

import daf.alerts.cache.postgres.api.RichPostgresMap;
import daf.alerts.common.models.Payload;
import daf.alerts.common.models.schema.AlertUrgencyLevelRefSchema;
import daf.alerts.common.models.schema.VehicleAlertRefSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Optional;

import static daf.alerts.common.props.AlertConfigProp.ALERT_THRESHOLD_FETCH_SINGLE_QUERY;
import static daf.alerts.common.props.AlertConfigProp.DATABASE;
import static daf.alerts.common.props.AlertConfigProp.DRIVER;
import static daf.alerts.common.props.AlertConfigProp.JDBC_URL;
import static daf.alerts.common.props.AlertConfigProp.PASSWORD;
import static daf.alerts.common.props.AlertConfigProp.USERNAME;

public class RichPostgresMapImpl extends RichPostgresMap<Payload> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Connection connection;

    private static final Logger logger = LoggerFactory.getLogger(RichPostgresMapImpl.class);


    public RichPostgresMapImpl(ParameterTool parameterTool){
        super(parameterTool);
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        logger.trace("DB connection open :: "+parameterTool.getProperties());
        Class.forName(parameterTool.get(DRIVER));
        String connectionUrl = parameterTool.get(JDBC_URL) + parameterTool.get(DATABASE) + "?" + "user=" + parameterTool.get(USERNAME) + "&" + "password=" + parameterTool.get(PASSWORD);
        connection = DriverManager.getConnection(connectionUrl);
    }

    @Override
    public Payload map(Payload payload) throws Exception {
        VehicleAlertRefSchema vehicleAlertRefSchema = (VehicleAlertRefSchema)payload.getData().get();
        AlertUrgencyLevelRefSchema schema = new AlertUrgencyLevelRefSchema();
        ResultSet resultSet = connection.createStatement()
                .executeQuery(parameterTool.get(ALERT_THRESHOLD_FETCH_SINGLE_QUERY) + "" + vehicleAlertRefSchema.getAlertId());
        while (resultSet != null && resultSet.next()){
            schema.withId(resultSet.getLong(1))
                    .withAlertId(resultSet.getLong(2))
                    .withUrgencyLevelType(resultSet.getString(3))
                    .withThresholdValue(resultSet.getLong(4))
                    .withUnitType(resultSet.getString(5))
                    .withState(resultSet.getString(6));
        }
        logger.info("record fetch from database :: "+schema.toString());
        return Payload.builder().data(Optional.of(Tuple2.of(vehicleAlertRefSchema,schema))).build();
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}
