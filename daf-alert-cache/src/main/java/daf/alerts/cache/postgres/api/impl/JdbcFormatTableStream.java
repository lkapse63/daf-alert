package daf.alerts.cache.postgres.api.impl;


import daf.alerts.cache.postgres.api.TableStream;
import daf.alerts.common.models.Payload;
import daf.alerts.common.models.schema.AlertUrgencyLevelRefSchema;
import daf.alerts.common.models.schema.VehicleAlertRefSchema;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.io.jdbc.JDBCInputFormat;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.types.Row;
import java.io.Serializable;
import java.time.Duration;
import java.util.Optional;

import static daf.alerts.common.props.AlertConfigProp.DATABASE;
import static daf.alerts.common.props.AlertConfigProp.DRIVER;
import static daf.alerts.common.props.AlertConfigProp.JDBC_URL;
import static daf.alerts.common.props.AlertConfigProp.PASSWORD;
import static daf.alerts.common.props.AlertConfigProp.USERNAME;


public class JdbcFormatTableStream extends TableStream<Row> implements Serializable {

    private static final long serialVersionUID = 1637717303256833931L;

    public JdbcFormatTableStream(final StreamExecutionEnvironment env,ParameterTool parameters){
        super(env,parameters);
    }

    @Override
    public DataStreamSource<Row> scanTable(String fetchQuery, TypeInformation<?>[] typeInfo,String database) {
        RowTypeInfo rowTypeInfo = new RowTypeInfo(typeInfo);
        JDBCInputFormat jdbcInputFormat = JDBCInputFormat.buildJDBCInputFormat()
                .setDrivername(parameters.get(DRIVER))
                .setDBUrl(parameters.get(JDBC_URL)+database)
                .setUsername(parameters.get(USERNAME))
                .setPassword(parameters.get(PASSWORD))
                .setQuery(fetchQuery)
                .setRowTypeInfo(rowTypeInfo)
                .finish();
        return env.createInput(jdbcInputFormat);
    }

    @Override
    public DataStream<Payload>joinTable(DataStreamSource<Row> first, DataStreamSource<Row> second) {

        SingleOutputStreamOperator<AlertUrgencyLevelRefSchema> alertUrgencyStream = first.map(row -> new AlertUrgencyLevelRefSchema()
                .withId(Long.valueOf(String.valueOf(row.getField(0))))
                .withAlertId(Long.valueOf(String.valueOf(row.getField(1))))
                .withUrgencyLevelType(String.valueOf(row.getField(2)))
                .withThresholdValue(Long.valueOf(String.valueOf(row.getField(3))))
                .withUnitType(String.valueOf(row.getField(4)))
                .withState(String.valueOf(row.getField(5)))
        )
                .returns(AlertUrgencyLevelRefSchema.class)
                .keyBy(alertUrgencyLevelRefSchema -> alertUrgencyLevelRefSchema.getAlertId())
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy
                                .<AlertUrgencyLevelRefSchema>forBoundedOutOfOrderness(Duration.ofSeconds(60))
                                .withTimestampAssigner((alertUrgencyLevelRefSchema, l) -> alertUrgencyLevelRefSchema.getTimestamp())
                );

        SingleOutputStreamOperator<VehicleAlertRefSchema> vehicleMapStream = second.map(row -> new VehicleAlertRefSchema()
                .withId(Long.valueOf(String.valueOf(row.getField(0))))
                .withState("A")
                .withAlertId(Long.valueOf(String.valueOf(row.getField(2))))
                .withVin(String.valueOf(row.getField(1)))
        )
                .returns(VehicleAlertRefSchema.class)
                .keyBy(vehicleAlertRefSchema -> vehicleAlertRefSchema.getAlertId())
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy
                                .<VehicleAlertRefSchema>forBoundedOutOfOrderness(Duration.ofSeconds(60))
                                .withTimestampAssigner((vehicleAlertRefSchema, l) -> vehicleAlertRefSchema.getTimestamp()));

        DataStream<Payload> joinStream = vehicleMapStream.join(alertUrgencyStream)
                .where(VehicleAlertRefSchema::getAlertId)
                .equalTo(AlertUrgencyLevelRefSchema::getAlertId)
                .window(TumblingEventTimeWindows.of(Time.seconds(60)))
                .apply(
                        new JoinFunction<VehicleAlertRefSchema, AlertUrgencyLevelRefSchema, Payload>() {
                            @Override
                            public Payload join(VehicleAlertRefSchema vehicleAlertRefSchema, AlertUrgencyLevelRefSchema alertUrgencyLevelRefSchema) throws Exception {
                                return Payload.builder().data(Optional.of(Tuple2.of(vehicleAlertRefSchema,alertUrgencyLevelRefSchema))).build();
                            }
                        }
                )
                .keyBy(payload -> ((VehicleAlertRefSchema)((Tuple2)payload.getData().get()).f0).getAlertId());

        return joinStream;
    }
    

}
