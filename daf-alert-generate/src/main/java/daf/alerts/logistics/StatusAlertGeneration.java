package daf.alerts.logistics;

import daf.alerts.cache.postgres.api.TableStream;
import daf.alerts.cache.postgres.api.impl.JdbcFormatTableStream;
import daf.alerts.cache.postgres.api.impl.RichPostgresMapImpl;
import daf.alerts.common.models.Alert;
import daf.alerts.common.models.Payload;
import daf.alerts.common.models.conti.Status;
import daf.alerts.common.models.schema.VehicleAlertRefSchema;
import daf.alerts.common.serialization.PojoKafkaSerializationSchema;
import daf.alerts.service.impl.AlertKeyedServiceLogistic;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static daf.alerts.common.props.AlertConfigProp.ALERT_MAP_FETCH_QUERY;
import static daf.alerts.common.props.AlertConfigProp.ALERT_MAP_SCHEMA_DEF;
import static daf.alerts.common.props.AlertConfigProp.ALERT_THRESHOLD_FETCH_QUERY;
import static daf.alerts.common.props.AlertConfigProp.ALERT_THRESHOLD_SCHEMA_DEF;
import static daf.alerts.common.props.AlertConfigProp.DATABASE;
import static daf.alerts.common.props.AlertConfigProp.DATABASE2;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_AUTO_OFFSET;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_BOOTSTRAP_SERVER;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_DAF_ALERT_MAPPING_TOPIC;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_DAF_ALERT_PRODUCE_MSG_TOPIC;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_DAF_STATUS_MSG_TOPIC;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_GRP_ID;
import static daf.alerts.common.props.AlertConfigProp.OUTPUT_TAG;
import static daf.alerts.common.props.AlertConfigProp.alertMappingSchemaMapStateDescriptor;
import static daf.alerts.common.util.Utils.readValueAsObject;
import static daf.alerts.functions.LogisticAlertFunction.excessiveGlobalMileage;

public class StatusAlertGeneration implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(StatusAlertGeneration.class);
    private static final long serialVersionUID = 1L;

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        ParameterTool parameterTool = ParameterTool.fromArgs(args);
//        PropertyConfigurator.configure(parameterTool.get("log4j"));
        logger.info("Application started with properties :: {}", parameterTool.getProperties());

        /**
         * Creating param tool from given property
         */
        ParameterTool propertiesParamTool = ParameterTool.fromPropertiesFile(parameterTool.get("prop"));
        env.getConfig().setGlobalJobParameters(propertiesParamTool);

        /**
         * Create kafka properties
         */
        Properties kafkaTopicProp = new Properties();
        kafkaTopicProp.put(KAFKA_GRP_ID, propertiesParamTool.get(KAFKA_GRP_ID));
        kafkaTopicProp.put(KAFKA_BOOTSTRAP_SERVER, propertiesParamTool.get(KAFKA_BOOTSTRAP_SERVER));
        kafkaTopicProp.setProperty(KAFKA_AUTO_OFFSET, propertiesParamTool.get(KAFKA_AUTO_OFFSET,"earliest"));


        /**
         * KAFKA topic configuration
         */
        FlinkKafkaConsumer<String> kafkaConsumerAlertMapCdcTopic = new FlinkKafkaConsumer<>(propertiesParamTool.get(KAFKA_DAF_ALERT_MAPPING_TOPIC),new SimpleStringSchema(),kafkaTopicProp);
        FlinkKafkaConsumer<String> kafkaContiMessageConsumer = new FlinkKafkaConsumer<>(propertiesParamTool.get(KAFKA_DAF_STATUS_MSG_TOPIC),new SimpleStringSchema(),kafkaTopicProp);
        String dafAlertProduceTopic = propertiesParamTool.get(KAFKA_DAF_ALERT_PRODUCE_MSG_TOPIC);
        FlinkKafkaProducer<Alert> alertProducerTopic  = new FlinkKafkaProducer<Alert>(dafAlertProduceTopic,new PojoKafkaSerializationSchema(dafAlertProduceTopic),kafkaTopicProp,FlinkKafkaProducer.Semantic.EXACTLY_ONCE);


        /**
         * Logistics functions defined
         */
         Map<Object, Object> configMap = new HashMap() {{
            put("functions", Arrays.asList(
                    excessiveGlobalMileage
            ));
        }};

        /**
         * Read update event from kafka topic
         */
        SingleOutputStreamOperator<Payload> kafkaAlertMapTopic = env.addSource(kafkaConsumerAlertMapCdcTopic)
                .map(json -> Payload.builder().data(Optional.of((VehicleAlertRefSchema) readValueAsObject(json, VehicleAlertRefSchema.class))).build())
                .returns(Payload.class);

        /**
         * Fetch latest updated record from data-mart
         */
        SingleOutputStreamOperator<Payload> updateStream = kafkaAlertMapTopic
                .keyBy(payload -> ((VehicleAlertRefSchema) payload.getData().get()).getAlertId())
                .map(new RichPostgresMapImpl(propertiesParamTool))
                .returns(Payload.class);

        /**
         * Scan and join the table
         */
        TableStream tableStream = new JdbcFormatTableStream(env, propertiesParamTool);

        DataStreamSource dataStreamSourceAlertMap = tableStream.scanTable(propertiesParamTool.get(ALERT_MAP_FETCH_QUERY), ALERT_MAP_SCHEMA_DEF, propertiesParamTool.get(DATABASE));
        DataStreamSource dataStreamSourceThreshold = tableStream.scanTable(propertiesParamTool.get(ALERT_THRESHOLD_FETCH_QUERY), ALERT_THRESHOLD_SCHEMA_DEF,propertiesParamTool.get(DATABASE2));

        DataStream joinTable = tableStream.joinTable(dataStreamSourceThreshold, dataStreamSourceAlertMap);

        /**
         * Broadcast the stream
         */
        BroadcastStream<Payload> broadcastAlertMappingStream = updateStream.union(joinTable)
                .broadcast(alertMappingSchemaMapStateDescriptor);

        /**
         * Process message for alert
         */
        SingleOutputStreamOperator<Payload> logisticsAlertStream = env.addSource(kafkaContiMessageConsumer)
                .map(json -> (Status) readValueAsObject(json, Status.class))
                .returns(Status.class)
                .keyBy(status -> status.getVin())
                .connect(broadcastAlertMappingStream)
                .process(new AlertKeyedServiceLogistic(configMap));

        /**
         * Publish alert on kafka topic
         */
        logisticsAlertStream
                .getSideOutput(OUTPUT_TAG)
                .addSink(alertProducerTopic);

        env.execute();

    }
}
