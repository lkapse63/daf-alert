package daf.alerts.logistics;

import daf.alerts.cache.postgres.api.TableStream;
import daf.alerts.cache.postgres.api.impl.JdbcFormatTableStream;
import daf.alerts.common.models.Alert;
import daf.alerts.common.models.Payload;
import daf.alerts.common.models.conti.Status;
import daf.alerts.common.models.schema.AlertThresholdVinMappingSchema;
import daf.alerts.common.props.AlertConfigProp;
import daf.alerts.common.serialization.PojoKafkaSerializationSchema;
import daf.alerts.service.impl.AlertKeyedServiceLogistic;
import daf.alerts.sources.LogisticAlertSource;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static daf.alerts.common.props.AlertConfigProp.ALERT_DB_VIN_THRESHOLD_SCHEMA_DEF;
import static daf.alerts.common.props.AlertConfigProp.DATABASE;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_BOOTSTRAP_SERVER;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_DAF_ALERT_PRODUCE_MSG_TOPIC;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_DAF_STATUS_MSG_TOPIC;
import static daf.alerts.common.props.AlertConfigProp.KAFKA_GRP_ID;
import static daf.alerts.common.props.AlertConfigProp.OUTPUT_TAG;
import static daf.alerts.common.props.AlertConfigProp.POSTGRESS_GROUP_VEHICLE_QUERY;
import static daf.alerts.common.props.AlertConfigProp.POSTGRESS_SINGLE_VEHICLE_QUERY;
import static daf.alerts.common.util.Utils.readValueAsObject;
import static daf.alerts.functions.LogisticAlertFunction.excessiveGlobalMileage;

public class AlertProcessing {

    private static final Logger logger = LoggerFactory.getLogger(AlertProcessing.class);
    private static final long serialVersionUID = 1L;

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        ParameterTool parameterTool = ParameterTool.fromArgs(args);
        logger.info("Application started with properties :: {}", parameterTool.getProperties());
        /**
         * Creating param tool from given property
         */
        ParameterTool propertiesParamTool = ParameterTool.fromPropertiesFile(parameterTool.get("prop"));
        env.getConfig().setGlobalJobParameters(propertiesParamTool);

        /**
         * Create kafka properties
         */
        Properties kafkaTopicProp = new Properties();
        kafkaTopicProp.put(KAFKA_GRP_ID, propertiesParamTool.get(KAFKA_GRP_ID));
        kafkaTopicProp.put(KAFKA_BOOTSTRAP_SERVER, propertiesParamTool.get(KAFKA_BOOTSTRAP_SERVER));

        FlinkKafkaConsumer<String> kafkaContiMessageConsumer = new FlinkKafkaConsumer<>(propertiesParamTool.get(KAFKA_DAF_STATUS_MSG_TOPIC), new SimpleStringSchema(), kafkaTopicProp);
        String dafAlertProduceTopic = propertiesParamTool.get(KAFKA_DAF_ALERT_PRODUCE_MSG_TOPIC);
        FlinkKafkaProducer<Alert> alertProducerTopic = new FlinkKafkaProducer<Alert>(dafAlertProduceTopic, new PojoKafkaSerializationSchema(dafAlertProduceTopic), kafkaTopicProp, FlinkKafkaProducer.Semantic.EXACTLY_ONCE);


        /**
         * Logistics functions defined
         */
        Map<Object, Object> configMap = new HashMap() {{
            put("functions", Arrays.asList(
                    excessiveGlobalMileage
            ));
        }};

        /**
         * Scan and join the table
         */
        TableStream tableStream = new JdbcFormatTableStream(env, propertiesParamTool);

        DataStreamSource dataStreamSingle = tableStream.scanTable(propertiesParamTool.get(POSTGRESS_SINGLE_VEHICLE_QUERY), ALERT_DB_VIN_THRESHOLD_SCHEMA_DEF, propertiesParamTool.get(DATABASE));
        DataStreamSource dataStreamGroup = tableStream.scanTable(propertiesParamTool.get(POSTGRESS_GROUP_VEHICLE_QUERY), ALERT_DB_VIN_THRESHOLD_SCHEMA_DEF, propertiesParamTool.get(DATABASE));

        BroadcastStream broadcastStream = dataStreamSingle.union(dataStreamGroup)
                .map(row -> {
                            Row row1 = (Row) row;
                            AlertThresholdVinMappingSchema schema = AlertThresholdVinMappingSchema.builder()
                                    .vin(String.valueOf(row1.getField(0)))
                                    .vid(String.valueOf(row1.getField(1)))
                                    .alertId(Integer.valueOf(String.valueOf(row1.getField(2))))
                                    .thresholdValue(Double.valueOf(String.valueOf(row1.getField(3))))
                                    .unitType(String.valueOf(row1.getField(4)))
                                    .urgencyLevelType(String.valueOf(row1.getField(5)))
                                    .alertName(String.valueOf(row1.getField(6)))
                                    .alertNameType(String.valueOf(row1.getField(7)))
                                    .parentEnum(String.valueOf(row1.getField(8)))
                                    .build();
                            return Payload.builder().data(Optional.of(schema)).build();
                        }
                )
                .broadcast(AlertConfigProp.alertMappingSchemaMapStateDescriptor);

        SingleOutputStreamOperator logisticsStream = env.addSource(kafkaContiMessageConsumer)
                .map(json -> (Status) readValueAsObject(json, Status.class))
                .returns(Status.class)
                .keyBy(status -> status.getVin())
                .connect(broadcastStream)
                .process(new AlertKeyedServiceLogistic(configMap))
                .returns(Object.class);

        logisticsStream
                .getSideOutput(OUTPUT_TAG)
                .addSink(alertProducerTopic);

        env.execute();

    }
}
