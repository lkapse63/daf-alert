package daf.alerts.sources;

import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

public class LogisticAlertSource extends RichSourceFunction<String> {

    private volatile boolean isRunning = true;

    @Override
    public void run(SourceContext<String> sourceContext) throws Exception {

        String data = "{\"GPSStartLatitude\": \"51.43042755\",\"VStartFuel\": \"116939374\",\"Increment\": \"54684\",\"GPSStartLongitude\": \"5.51616478\",\"DocFormat\": \"json\",\"EventDateTimeFirstIndex\": \"2021-05-03T13:11:31.000Z\",\"TransID\": \"03010\",\"GPSEndLongitude\": \"5.508029461\",\"TenantID\": \"DAF_4A\",\"VStartTankLevel\": \"55\",\"DocVersion\": \"1.3\",\"Reserve0\": \"10000\",\"GPSStopVehDist\": \"418215680\",\"ROProfil\": \"BASE_CONTI\",\"ROrelease\": \"13.1.1_M4A\",\"VStopTankLevel\": \"55\",\"VNegAltitudeVariation\": \"6\",\"Jobname\": \"acco07\",\"GPSStartVehDist\": \"418214765\",\"VPosAltitudeVariation\": \"0\",\"GPSEndDateTime\": \"2021-05-03T13:14:02.000Z\",\"NumSeq\": \"997\",\"VUsedFuel\": \"342\",\"VCruiseControlDist\": \"0\",\"ROmodel\": \"TCU Conti Flex 4A\",\"VHarshBrakeDuration\": \"5\",\"VPTOCnt\": \"0\",\"TargetM2M\": \"MOM_CBP\",\"VStopFuel\": \"116939716\",\"Reserve1\": \"1\",\"PartitionID\": \"8\",\"DriverID\": \"NL B000384974000000\",\"VBrakeDuration\": \"17\",\"ROName\": \"CONTI_FLEX_181812081514311400938\",\"VEvtID\": \"5\",\"GPSEndLatitude\": \"51.43199539\",\"GPSStartDateTime\": \"2021-05-03T13:11:00.000Z\",\"NumberOfIndexMessage\": \"2\",\"vin\": \"abc\",\"VIdleDuration\": \"14\",\"VPTODuration\": \"0\",\"VPTODist\": \"0\",\"messageKey\": \"M4A1117_1620047672_03010\",\"receivedTimestamp\": 1620047672650,\"hostname\": \"tsclba64s\",\"partitionId\": \"8\",\"queue\": \"MSGTOCBP08_4A\"}";
        while (isRunning) {
            sourceContext.collect(data);
            Thread.sleep(1000);
        }

    }

    @Override
    public void cancel() {

        isRunning = false;

    }
}
