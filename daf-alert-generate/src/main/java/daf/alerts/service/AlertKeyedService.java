package daf.alerts.service;

import java.io.Serializable;
import java.util.Map;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;

public abstract class AlertKeyedService<KS, IN1, IN2, OUT> extends KeyedBroadcastProcessFunction<KS, IN1, IN2, OUT> implements Serializable {
    private static final long serialVersionUID = 1637717303256833931L;

    protected Map<Object, Object> configMap;
    protected ParameterTool parameterTool;

    public AlertKeyedService(){

    }

    public AlertKeyedService(Map<Object, Object> configMap, ParameterTool parameterTool) {
        this.configMap = configMap;
        this.parameterTool = parameterTool;
    }

    public AlertKeyedService(Map<Object, Object> configMap) {
        this.configMap = configMap;
    }
}
