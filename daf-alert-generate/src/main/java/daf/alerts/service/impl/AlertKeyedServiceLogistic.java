package daf.alerts.service.impl;

import daf.alerts.common.models.Payload;
import daf.alerts.common.models.conti.Status;
import daf.alerts.common.models.schema.AlertThresholdVinMappingSchema;
import daf.alerts.common.models.schema.AlertUrgencyLevelRefSchema;
import daf.alerts.config.AlertConfig;
import daf.alerts.service.AlertKeyedService;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static daf.alerts.common.props.AlertConfigProp.OUTPUT_TAG;
import static daf.alerts.common.props.AlertConfigProp.alertMappingSchemaMapStateDescriptor;

public class AlertKeyedServiceLogistic extends AlertKeyedService<Object, Status, Payload, Payload> {

    private static final Logger logger = LoggerFactory.getLogger(AlertKeyedServiceLogistic.class);


    public AlertKeyedServiceLogistic(Map<Object, Object> configMap){
        super(configMap);
    }

    @Override
    public void processElement(Status status, ReadOnlyContext readOnlyContext, Collector<Payload> collector) throws Exception {
        logger.info("processElement status :: " + status);

        ReadOnlyBroadcastState<String, Payload> broadcastState = readOnlyContext.getBroadcastState(alertMappingSchemaMapStateDescriptor);

        if (broadcastState.contains(status.getVin())) {
            Payload payload = broadcastState.get(status.getVin());
            Set<AlertUrgencyLevelRefSchema> vinAlertList = (Set<AlertUrgencyLevelRefSchema>) payload.getData().get();

            Set<AlertUrgencyLevelRefSchema> refSchemas = vinAlertList.stream()
                    .filter(schema -> schema.getAlertTypeName().toLowerCase().contains("excessiveGlobalMileage".toLowerCase()))
                    .collect(Collectors.toSet());

            Map<String, Object> functionThresh = new HashMap<>();
            functionThresh.put("excessiveGlobalMileage", refSchemas);


            AlertConfig
                    .buildMessage(status, configMap, functionThresh)
                    .process()
                    .getAlert()
                    .ifPresent(
                            alerts -> {
                                alerts.stream()
                                        .forEach(alert -> readOnlyContext.output(OUTPUT_TAG, alert));
                            }
                    );
            logger.info("alert process for  {} check those alerts {}",status,refSchemas);
        }

    }

    @Override
    public void processBroadcastElement(Payload payload, Context context, Collector<Payload> collector) throws Exception {
        BroadcastState<String, Payload> broadcastState = context.getBroadcastState(alertMappingSchemaMapStateDescriptor);
        Set<AlertUrgencyLevelRefSchema> vinAlertList = new HashSet<>();
        AlertThresholdVinMappingSchema schema = (AlertThresholdVinMappingSchema) payload.getData().get();
        AlertUrgencyLevelRefSchema refSchema = new AlertUrgencyLevelRefSchema();

        refSchema.withAlertId(Long.valueOf(schema.getAlertId()))
                .withUnitType(schema.getUnitType())
                .withThresholdValue(schema.getThresholdValue().longValue())
                .withUrgencyLevelType(schema.getUrgencyLevelType())
                .withAlertTypeName(schema.getAlertNameType())
                .withAlertName(schema.getAlertName());

        if(broadcastState.contains(schema.getVin())){
            Payload listPayload = broadcastState.get(schema.getVin());
            vinAlertList = (Set<AlertUrgencyLevelRefSchema>) listPayload.getData().get();
            vinAlertList.add(refSchema);
            broadcastState.put(schema.getVin(), Payload.builder().data(Optional.of(vinAlertList)).build());
        }else{
            vinAlertList.add(refSchema);
            broadcastState.put(schema.getVin(), Payload.builder().data(Optional.of(vinAlertList)).build());
        }
        logger.info("processBroadcastElement payload :: {}",  payload);
    }
}
