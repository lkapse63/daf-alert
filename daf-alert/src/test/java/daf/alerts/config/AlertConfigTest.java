package daf.alerts.config;

import daf.alerts.common.models.Payload;
import daf.alerts.models.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Optional;

import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class AlertConfigTest {

    @Test
    public void buildMessage() {
        Message message = AlertConfig.buildMessage(Payload.builder().data(Optional.of("test-data")).build(), new HashMap<>(), new HashMap<>());
        Assert.assertNotNull(message.getPayload().get());
        Assert.assertEquals("test-data", ((Payload)message.getOriginalPayload()).getData().get());
    }

    @Test
    public void testBuildMessage() {
        Message message = AlertConfig.buildMessage(Payload.builder().data(Optional.of("test-data")).build(), new HashMap<>());
        Assert.assertNotNull(message.getPayload().get());
        Assert.assertEquals("test-data", ((Payload)message.getOriginalPayload()).getData().get());

        message = AlertConfig.buildMessage(Payload.builder().data(Optional.of("test-data")).build(), new HashMap<>(), null);
        Assert.assertNotNull(message.getPayload().get());
        Assert.assertEquals("test-data", ((Payload)message.getOriginalPayload()).getData().get());
    }
}