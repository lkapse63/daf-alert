package daf.alerts.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class AlertExecutorFactoryTest {

    @Test
    public void getAlertExecutorConfig() {

        AlertExecutorConfig alertExecutorConfig = AlertExecutorFactory.getAlertExecutorConfig();
        Assert.assertNotNull(alertExecutorConfig);
    }
}