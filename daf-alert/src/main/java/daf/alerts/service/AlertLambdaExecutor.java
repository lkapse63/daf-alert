package daf.alerts.service;

import java.io.Serializable;

@FunctionalInterface
public interface AlertLambdaExecutor<T,R> extends Serializable {

    R apply(T source);
}
