package daf.alerts.functions;

import daf.alerts.common.models.Alert;
import daf.alerts.common.models.Target;
import daf.alerts.common.models.conti.Status;
import daf.alerts.common.models.schema.AlertUrgencyLevelRefSchema;
import daf.alerts.models.Message;
import daf.alerts.service.AlertLambdaExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

public class LogisticAlertFunction implements Serializable {

    private static final long serialVersionUID = -2623908626314058510L;
    private static final Logger logger = LoggerFactory.getLogger(LogisticAlertFunction.class);

    public static AlertLambdaExecutor<Message, Target> excessiveDistanceDone  = (Message s) -> {
        Status status = (Status) s.getPayload().get();
        Double value = Double.valueOf(status.getGPSStopVehDist()) - Double.valueOf(status.getGPSStartVehDist());
        Predicate<Double> isGreater = (Double v) -> thresholdBreach(v, 5.0);
        if(isGreater.test(value))
            return Target.builder()
                    .alert(Optional.of(Alert.builder()
                            .id("1")
                            .vin("12345")
                            .categoryType("L")
                            .alertid("26")
                            .latitude(status.getGPSEndLongitude())
                            .longitude(status.getGPSEndLongitude())
                            .alertGeneratedTime(String.valueOf(System.currentTimeMillis()))
                            .thresholdValue("5.0")
                            .build()))
                    .build();
        else
            return Target.builder().metaData(s.getMetaData()).payload(s.getPayload()).alert(Optional.empty()).build();
    };


    public static AlertLambdaExecutor<Message,Target> excessiveGlobalMileage = (Message s) -> {
        Status status = (Status) s.getPayload().get();
        Map<String,Object> threshold = (Map<String, Object>) s.getMetaData().getThreshold().get();
        Set<AlertUrgencyLevelRefSchema> urgencyLevelRefSchemas = (Set<AlertUrgencyLevelRefSchema>) threshold.get("excessiveGlobalMileage");

        for(AlertUrgencyLevelRefSchema urgency : urgencyLevelRefSchemas){

            if(urgency.getUrgencyLevelType().equalsIgnoreCase("C")){
                if(thresholdBreach(Double.valueOf(status.getGPSStopVehDist()), Double.valueOf(urgency.getThresholdValue()))){
                    logger.info("alert found excessiveGlobalMileage ::type {} , threshold {} , status {}",urgency.getAlertTypeName(),urgency.getThresholdValue(),status);
                    return getTarget(status, urgency);
                }
            }
            if(urgency.getUrgencyLevelType().equalsIgnoreCase("W")){
                if(thresholdBreach(Double.valueOf(status.getGPSStopVehDist()), Double.valueOf(urgency.getThresholdValue()))){
                    logger.info("alert found excessiveGlobalMileage ::type {} , threshold {} , status {}",urgency.getAlertTypeName(),urgency.getThresholdValue(),status);
                    return getTarget(status, urgency);
                }
            }
            if(urgency.getUrgencyLevelType().equalsIgnoreCase("A")){
                if(thresholdBreach(Double.valueOf(status.getGPSStopVehDist()), Double.valueOf(urgency.getThresholdValue()))){
                    logger.info("alert found excessiveGlobalMileage ::type {} , threshold {} , status {}",urgency.getAlertTypeName(),urgency.getThresholdValue(),status);
                    return getTarget(status, urgency);
                }
            }
        }

        return Target.builder().metaData(s.getMetaData()).payload(s.getPayload()).alert(Optional.empty()).build();
    };

    private static boolean thresholdBreach(Double aDouble, Double thresholdValue) {
        return aDouble > thresholdValue;
    }

    private static Target getTarget(Status status, AlertUrgencyLevelRefSchema urgency) {
        return Target.builder()
                .alert(Optional.of(Alert.builder()
                        .id("" + urgency.getId())
                        .vin(status.getVin())
                        .categoryType("L")
                        .alertid("" + urgency.getAlertId())
                        .latitude(status.getGPSEndLongitude())
                        .longitude(status.getGPSEndLongitude())
                        .alertGeneratedTime(String.valueOf(System.currentTimeMillis()))
                        .thresholdValue("" + urgency.getThresholdValue())
                        .thresholdValueUnitType(urgency.getUnitType())
                        .name(urgency.getAlertName())
                        .valueAtAlertTime(status.getGPSStopVehDist())
                        .build()))
                .build();
    }

}
